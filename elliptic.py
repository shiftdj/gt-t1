import random
from math import sin, cos, pi, sqrt
import numpy as np
import visvis as vv
from PyQt5 import QtGui, QtCore  # noqa
backend = 'pyqt5'


class Elipsoid:
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def coord(self, phi, theta):
        x = self.a * np.sin(phi) * np.cos(theta)
        y = self.a * np.sin(phi) * np.sin(theta)
        z = self.b * np.cos(phi)
        return x, y, z

    def generate_points(self, n, offset):
        indices = np.arange(0, n, dtype=float) + offset

        phi = np.arccos(2 * indices / (n + offset) - 1)
        theta = pi * (1 + sqrt(5)) * indices

        return list(np.array(x) for x in zip(*self.coord(phi, theta)))

    def generate_random(self):
        phi = np.arccos(2 * np.random.random() - 1)
        theta = np.random.random() * 2 * pi

        return self.coord(phi, theta)


# Вводим данные
# ab = input('Enter a, b: ')
# rad = float(input('Enter radius: '))
# n = int(input('Enter count of base points: '))
# nsim = int(input('Enter simulation duration: '))

abc = '2 1'
rad = 1
n = 5
nsim = 10000


a, b = [float(x) for x in abc.split(' ')]
e = Elipsoid(a, b)

points = e.generate_points(n, np.random.random())

result = {
    True: 0,
    False: 0
}

for i in range(nsim):
    rand_point = e.generate_random()
    win = False
    for point in points:
        dist = np.linalg.norm(point - rand_point)
        if dist <= rad:
            win = True
            break
    result[win] += 1


cost = result[True] / nsim

print('Цена игры', result[True] / nsim)

r = np.linspace(0, pi, 20)
p = np.linspace(0, 2*pi, 20)
R, P = np.meshgrid(r, p)

X = a * np.sin(R) * np.cos(P)
Y = a * np.sin(R) * np.sin(P)
Z = b * np.cos(R)

app = vv.use(backend)
m = vv.surf(X, Y, Z)
m.colormap = vv.CM_WINTER

for point in points:
    sp = vv.solidSphere(tuple(point), rad)
    sp.specular = 0
    sp.diffuse = 1.0
    sp.faceColor = 'r'

app.Run()
